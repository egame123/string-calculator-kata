package com.example.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.example.service.StringCalcuator;

public class StringCalculatorTest {

	@Test
	public void returnZeroOnEmptyText() throws Exception {
		assertEquals(0, StringCalcuator.add(""));
	}

	@Test
	public void testAddOneNumberString() throws Exception {
		assertEquals(1, StringCalcuator.add("1"));
	}

	@Test
	public void testAddTwoNumberString() throws Exception {
		assertEquals(3, StringCalcuator.add("1,2"));
	}

	@Test
	public void testAddMultipleNumberString() throws Exception {
		assertEquals(10, StringCalcuator.add("1,2,3,4"));
	}

	@Test
	public void testAddRandomLengthNumberWithNewline() throws Exception {
		assertEquals(21, StringCalcuator.add("1,2,3,4\n5,6"));
	}

	@Test
	public void testAddRandomLengthNumberWithCustomdelimeter() throws Exception {
		assertEquals(21, StringCalcuator.add("//;\n1;2;3;4;5;6"));
	}

	@Test
	public void testAddOneNegativeNumberString() throws Exception {
		try {
			assertEquals(3, StringCalcuator.add("-1,2"));
			assertEquals(false, true);
		} catch (Exception e) {
			assertEquals(true, true);
		}
	}
	
	@Test
	public void testAddOneNegativeNumberStringWithMessage() throws Exception {
		try {
			assertEquals(3, StringCalcuator.add("-1,2,-3"));
			assertEquals(false, true);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "negatives not allowed -1,-3");
		}
	}
	
	@Test
	public void testAddGreaterThanThousand() throws Exception {
		assertEquals(3, StringCalcuator.add("2,1001"));
	}
	
	@Test
	public void testAddRandomLengthNumberWithCustomdelimeterSpecialCharacter() throws Exception {
		assertEquals(21, StringCalcuator.add("//*\n1*2*3*4*5*6"));
	}
	
	@Test
	public void testMultiLengthDelimeter() throws Exception {
		assertEquals(6, StringCalcuator.add("//[***]\n1***2***3"));
	}
	
	@Test
	public void testSingleLengthMultipleDelimeters() throws Exception {
		assertEquals(6, StringCalcuator.add("//[*][%]\n1*2%3"));
	}
	
	@Test
	public void testMultiLengthMultipleDelimeters() throws Exception {
		assertEquals(6, StringCalcuator.add("//[**][%]\n1**2%3"));
	}
	
	@Test
	public void testMultipleLengthMultipleDelimeters() throws Exception {
		assertEquals(6, StringCalcuator.add("//[**][%%]\n1**2%%3"));
	}
}
