package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StringCalcuator {

	public static int add(String text) throws Exception {
		if (text.isEmpty())
			return 0;
		text = removeSquareBraces(text);
		List<Integer> numbers = convertStringToNumbers(text);
		List<Integer> negativeNumbers = numbers.stream().filter(num -> num < 0).collect(Collectors.toList());
		if (negativeNumbers.size() > 0) {
			handleNegativeNumbers(negativeNumbers);
		}
		int sum = sum(numbers);
		return sum;
	}
	
	public static int sum(List<Integer> numbers) {
		int sum = numbers.stream().mapToInt(Integer::intValue).sum();
		return sum;
	}
	
	public static void handleNegativeNumbers(List<Integer> negativeNumbers) throws Exception {
		String commaSeperatedNegatives = String.join(",",
				negativeNumbers.stream().map(x -> String.valueOf(x)).collect(Collectors.toList()));
		throw new Exception("negatives not allowed " + commaSeperatedNegatives);
	}

	private static String removeSquareBraces(String text) {
		text = text.replaceAll("\\[", "");
		text = text.replaceAll("\\]", "");
		return text;
	}

	public static String[] splitString(String text) {
		String[] numberStringArray = null;
		String delimeter = ",|\n";
		if (text.startsWith("//")) {
			numberStringArray = handleCustomDelimeter(text);

		} else
			numberStringArray = text.split(delimeter);
		return numberStringArray;
	}

	public static String[] handleCustomDelimeter(String text) {
		String regex = "(//)(.+)(\\n)(.*)";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		String[] numberStringArray = null;
		if (matcher.find()) {
			String delimeter = matcher.group(2);
			text = matcher.group(4);
			if (delimeter.matches("(.)\\1+"))// repeating delimeter
				numberStringArray = text.split(Pattern.quote(delimeter));
			else
				numberStringArray = text.split("[" + Pattern.quote(delimeter) + "]");

		}
		return numberStringArray;
	}

	public static List<Integer> convertStringToNumbers(String text) {
		String[] numbers = splitString(text);
		List<Integer> intNumbers = new ArrayList<Integer>();
		for (String numberString : numbers) {
			if (numberString.isEmpty())
				continue;
			intNumbers.add(toInteger(numberString));
		}
		return intNumbers;
	}

	public static int toInteger(String number) {
		int num = Integer.valueOf(number);
		return num % 1000;
	}
}
